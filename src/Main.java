import java.util.Random;
import java.util.Scanner;

public class Main {
    private static String name;
    private static Scanner sc = new Scanner(System.in);
    private static double rating = 1500;
    private static Random rand = new Random();
    private static int ratingToGive = rand.nextInt(7);
    private static int givenRating = ratingToGive + 20;
    private static boolean running = true;

    public static void main(String args[]) throws InterruptedException {

        while (running == true) {
            rating = 3;
            System.out.println("Enter your name for score keeping : ");
            name = sc.nextLine();
            System.out.println("Hello " + name + " would you like to 1.Start Game , 2.View Current Rating, 3.View Guide, or 4.Exit Game");

            int selection;
            selection = sc.nextInt();
            switch (selection) {
                case 1:
                    running = false;
                case 2:
                    running = false;
                case 3:
                    running = false;
                case 4:
                    running = false;
            }
            if (selection == 1) {
                startGame();
            } else if (selection == 2) {
                viewRating();
            } else if (selection == 3) {
                viewGuide();
            } else if (selection == 4) {
                System.out.println("Exit");
            }
        }
    }

        public static void startGame () {
            //Debugging purposes, we show which case was selected.
            System.out.println("Selected 1");

            //We use the 'name' variable to be more personal, then we ask the user to choose a number from X and Y which in our case is 1-5
            System.out.println(name + " please guess a number from 1-5");

            //We make the integer variable 'guess' to represent the users guess.
            int guess;

            //We assign the 'guess' variable to the next users input
            guess = sc.nextInt();

            //We create the integer variable 'numberToGuess' to be used later to create a random number to be guessed by the user.
            int numberToGuess;

            /*
            Here we create a new random variable and assign the 'numberToGuess' variable a new random integer from X and Y which is just 5
            We needed to also import the random utility library needed for the random number generator(This can be found near the top)
            */
            Random rand = new Random();
            numberToGuess = rand.nextInt(5);


            if(guess == numberToGuess){
                rating = rating + givenRating;

            }
        }
        private static void viewRating () {
            System.out.println(rating);
            running = true;
        }

        private static void viewGuide () {

        }

        }


